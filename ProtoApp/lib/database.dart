import 'package:cloud_firestore/cloud_firestore.dart';

class Database {
  String uid;
  Database({this.uid});

  CollectionReference favoriteCollection = FirebaseFirestore.instance.collection('favorites');

  createFavorites() {
    favoriteCollection.doc(uid).set({
      'favorites': []
    }).then((value) => value);
  }

  addFavorite(String locationName) async {
    List favorites = await getFavorites;
    favorites.add(locationName);
    favoriteCollection.doc(uid).update({
      'favorites': favorites
    });
  }

  removeFavorite(List favorites) {
    favoriteCollection.doc(uid).update({
      'favorites': favorites
    });
  }

  get getFavorites {
    return favoriteCollection.doc(uid).get().then((doc) => doc.data()['favorites']);
  }
}