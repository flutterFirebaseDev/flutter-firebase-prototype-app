import 'package:fluttertoast/fluttertoast.dart';

showShortToast(String msg, int time) {
  Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: time);
}

showLongToast(String msg, int time) {
  Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_LONG, timeInSecForIosWeb: time);
}