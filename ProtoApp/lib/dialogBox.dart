import 'package:ProtoApp/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

String code = '';

signOutDialog(BuildContext context) {
  FirebaseAuth auth = FirebaseAuth.instance;
  showDialog(
      context: context,
      builder: (_) {
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            child: Container(
                margin: const EdgeInsets.all(8.0),
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Do you wish to Log out?', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        RaisedButton(
                          child: Text('Yes'),
                          onPressed: () async {
                            await auth.signOut();
                            showShortToast('Logged Out Successfully', 2000);
                            Navigator.pop(context);
                          },
                        ),
                        RaisedButton(
                            child: Text('No'),
                            onPressed: () {
                              showShortToast('Okay...', 1000);
                              Navigator.pop(context);
                            }
                        )
                      ],
                    )
                  ],
                )
            )
        );
      }
  );
}

loginDialog(BuildContext context) {
  FirebaseAuth auth = FirebaseAuth.instance;
  String _phoneNumber;

  showDialog(
      context: context,
      builder: (_) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.4,
            child: Container(
              margin: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  Text('Login To Proceed', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600)),
                  SizedBox(height: 10),
                  Text('Please enter your mobile number', style: TextStyle(fontSize: 14)),
                  SizedBox(height: 20),
                  InternationalPhoneNumberInput(
                    countries: ['US', 'IN'],
                    onInputChanged: (PhoneNumber number) {
                      _phoneNumber = '${number.dialCode} ${number.parseNumber()}';
                      },
                  ),
                  SizedBox(height: 10),
                  Align(
                    alignment: Alignment.center,
                    child: RaisedButton(
                      child: Text('Login'),
                      onPressed: () async {
                        await auth.verifyPhoneNumber(
                            phoneNumber: _phoneNumber,
                            timeout: Duration(seconds: 30),
                            verificationCompleted: (PhoneAuthCredential credential) async {
                              await auth.signInWithCredential(credential);
                              showLongToast('Logged In Successfully', 2000);
                              Navigator.pop(context);
                              },
                            verificationFailed: (FirebaseAuthException error) {
                              print(error.message);
                              },
                            codeSent: (String verificationId, int resendToken) {
                              showShortToast('SMS Sent', 1000);
                              },
                            codeAutoRetrievalTimeout: (String verificationId) {}
                            );
                        },
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            )
          ),
        );
      }
  );
}

codeInputDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (_) {
      return Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))
        ),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Enter Your OTP Here'
                ),
                onChanged: (String value) {
                  code = value;
                },
              ),
              RaisedButton(
                child: Text('Submit'),
                onPressed: () {

                },
              )
            ],
          )
        )
      );
    }
  );
}