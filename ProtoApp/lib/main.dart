import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:ProtoApp/homePage.dart';
import 'package:flutter/services.dart';
import 'package:theme_provider/theme_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return ThemeProvider(
      saveThemesOnChange: true,
      loadThemeOnInit: true,
      defaultThemeId: 'custom_light_theme',
      themes: [
        AppTheme(
          id: 'custom_light_theme',
          description: 'A custom light theme',
          data: ThemeData(
              primarySwatch: Colors.indigo,
              buttonTheme: ButtonThemeData(
                  buttonColor: Colors.indigoAccent,
                  textTheme: ButtonTextTheme.values[2]
              ),
              bottomNavigationBarTheme: BottomNavigationBarThemeData(
                  selectedItemColor: Colors.indigoAccent,
                  unselectedItemColor: Colors.grey[600],
                  showUnselectedLabels: true
              ),
              brightness: Brightness.light
          )
        ),
        AppTheme(
          id: 'custom_dark_theme',
          description: 'A custom dark theme',
          data: ThemeData(
            brightness: Brightness.dark,
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey[500],
              showUnselectedLabels: true
            )
          )
        )
      ],
      child: ThemeConsumer(
        child: Builder(
          builder: (themeContext) => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'ProtoApp',
            theme: ThemeProvider.themeOf(themeContext).data,
            home: StreamBuilder(
                stream: FirebaseAuth.instance.authStateChanges(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return HomePage('', null);
                  return StreamBuilder<DocumentSnapshot>(
                      stream: FirebaseFirestore.instance.collection('favorites').doc(FirebaseAuth.instance.currentUser.uid).snapshots(),
                      builder: (context, favSnap) {
                        if (!favSnap.hasData) return Container(child: Center(child: CircularProgressIndicator()));
                        return HomePage(FirebaseAuth.instance.currentUser.uid, favSnap.data.data() == null ? null : favSnap.data.data()['favorites']);
                      }
                  );
                }
            ),
          )
        )
      )
    );
  }
}