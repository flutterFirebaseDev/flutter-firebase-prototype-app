import 'package:ProtoApp/database.dart';
import 'package:ProtoApp/toast.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:ProtoApp/models/Location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ProtoApp/dialogBox.dart';
import 'package:theme_provider/theme_provider.dart';

class HomePage extends StatefulWidget {
  final String uid;
  final List favorites;
  HomePage(this.uid, this.favorites);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _currentIndex = 0;
  PageController _pageController = PageController();
  String location = '';

  @override
  Widget build(BuildContext context) {
    Theme.of(context).copyWith(brightness: Brightness.dark);
    String uid = widget.uid;
    List favorites = widget.favorites ?? [];

    return Scaffold(
      appBar: AppBar(
        title: Text('Select City'),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10.0),
            child: Row(
              children: [
                GestureDetector(
                  child: ThemeProvider.themeOf(context).id == 'custom_light_theme' ?
            Icon(Icons.lightbulb_outline, color: Colors.yellow) :
            Icon(Icons.lightbulb_outline),
                  onTap: () {
                    if (ThemeProvider.themeOf(context).id == 'custom_light_theme') ThemeProvider.controllerOf(context).setTheme('custom_dark_theme');
                    else ThemeProvider.controllerOf(context).setTheme('custom_light_theme');
                  },
                ),
                SizedBox(width: 20),
                widget.uid != '' ? GestureDetector(
                  child: Icon(Icons.exit_to_app, color: Colors.white),
                  onTap: () {
                    signOutDialog(context);
                  },
                ) : GestureDetector(
                  child: Icon(Icons.person_add, color: Colors.white),
                  onTap: () {
                    loginDialog(context);
                  },
                )
              ],
            )
          )
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance.collection('locations').snapshots(),
          builder: (context, locationSnap) {
            if (!locationSnap.hasData) return Center(child: Text('Loading...'));
            else if (locationSnap.hasError) {
              print(locationSnap.error);
              return null;
            }
            else {
              List<Location> locations = locationSnap.data.docs.map((doc) => Location(
                  name: doc.data()['name'],
                  imgUrl: doc.data()['imgUrl']
              )).toList();

              return PageView(
                controller: _pageController,
                physics: BouncingScrollPhysics(),
                children: [
                  Container(
                      padding: const EdgeInsets.all(10.0),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: locations.length,
                        itemBuilder: (context, index) {
                          return Container(
                              height: MediaQuery.of(context).size.height * 0.20,
                              child: GestureDetector(
                                child: Card(
                                  child: Stack(
                                    children: [
                                      Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: NetworkImage(locations[index].imgUrl)
                                              )
                                          )
                                      ),
                                      Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Text(locations[index].name, style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w700,
                                                shadows: <Shadow>[
                                                  Shadow(
                                                      offset: Offset(2.0, 2.0),
                                                      blurRadius: 1.0,
                                                      color: Colors.black
                                                  ),
                                                ]
                                            )),
                                          )
                                      ),
                                      Align(
                                          alignment: Alignment.topRight,
                                          child: widget.uid == '' ? GestureDetector(
                                            child: Icon(Icons.star_border, color: Colors.yellow),
                                            onTap: () {
                                              loginDialog(context);
                                            },
                                          ) : GestureDetector(
                                            child: favorites.contains(locations[index].name) ?
                                            Icon(Icons.star, color: Colors.yellow, size: 27) :
                                            Icon(Icons.star_border, color: Colors.yellow, size: 27),
                                            onTap: () async {
                                              if (widget.favorites == null) {
                                                await Database(uid: uid).createFavorites();
                                                await Database(uid: uid).addFavorite(locations[index].name);
                                              } else {
                                                if (favorites.contains(locations[index].name)) {
                                                  favorites.remove(locations[index].name);
                                                  await Database(uid: uid).removeFavorite(favorites);
                                                } else {
                                                  await Database(uid: uid).addFavorite(locations[index].name);
                                                }
                                              }
                                            },
                                          )
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    if (uid != '') _currentIndex = 1;
                                    else showShortToast('Please Login', 1000);
                                    location = locations[index].name;
                                  });
                                  if (uid != '') _pageController.animateToPage(_currentIndex, duration: Duration(milliseconds: 500), curve: Curves.ease);
                                  FirebaseAnalytics().logEvent(name: 'Visited', parameters: {'location': locations[index].name, 'uid': uid});
                                },
                              )
                          );
                        },
                      )
                  ),
                  Container(
                    child: Center(
                      child: Text('Welcome to $location', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25))
                    )
                  ),
                  Container(),
                  Container()
                ],
              );
            }
          }
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 10.0,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.explore),
              title: Text('Explore')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.people),
              title: Text('Discuss')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_pin),
              title: Text('User')
          )
        ],
        currentIndex: _currentIndex,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
            _pageController.animateToPage(_currentIndex, duration: Duration(milliseconds: 500), curve: Curves.ease);
          });
        },
      ),
    );
  }
}