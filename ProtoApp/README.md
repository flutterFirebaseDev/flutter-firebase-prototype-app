# ProtoApp

**ProtoApp** is a Flutter mobile application that contains a list of locations, with the selection of location opening up a page with a welcome message corresponding to that location. The data of locations comes directly from Firebase Firestore and is connected to Flutter in form of a Stream, allowing live changes to reflect on the app immediately. 

 The locations can be added to favorites using the star in the top right corner of each location card. If the user is not logged in, a login prompt will be shown where the user can login using his/her mobile number. 
 
Once verified, the user's favorite list of locations will be updated to their corresponding unique User ID (UID) in Firestore.

### Instructions

1. Clone the repository to a location on your device
2. Open up the terminal at the directory you cloned it to, and type in the following command `flutter run` to create a build and launch the app in debug mode (Assuming the Flutter SDK is already installed, and either the emulator or the device is running)
3. The login process can be done by tapping on the **top right corner icon** of the screen, or by attempting to favorite a location while not logged in
4. Choose your country and enter the mobile number and click on **Login**, a SMS will be sent containing the **OTP**, which will be automatically inputted by the device itself
5. You are now logged in, and can choose to favorite a location by tapping on the **star icon** in the top right corner of each location card
6. If you wish so, there is a **light/dark mode toggle** beside the login/signout button that toggles between dark and light mode
7. To log out, click the button in the top right corner again and in the ensuing dialog, click **Yes**